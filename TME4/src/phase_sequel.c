#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define TAGINIT    0
#define NB_SITE 6

#define DIAMETRE	5	/* !!!!! valeur a initialiser !!!!! */

void simulateur(void) {
    int i;

    /* nb_voisins_in[i] est le nombre de voisins entrants du site i */
    /* nb_voisins_out[i] est le nombre de voisins sortants du site i */
    int nb_voisins_in[NB_SITE+1] = {-1, 2, 1, 1, 2, 1, 1};
    int nb_voisins_out[NB_SITE+1] = {-1, 2, 1, 1, 1, 2, 1};

    int min_local[NB_SITE+1] = {-1, 4, 7, 1, 6, 2, 9};

    /* liste des voisins entrants */
    int voisins_in[NB_SITE+1][2] = {{-1, -1},
                                    {4, 5}, {1, -1}, {1, -1},
                                    {3, 5}, {6, -1}, {2, -1}};

    /* liste des voisins sortants */
    int voisins_out[NB_SITE+1][2] = {{-1, -1},
                                     {2, 3}, {6, -1}, {4, -1},
                                     {1, -1}, {1, 4}, {5,-1}};

    for(i=1; i<=NB_SITE; i++){
        MPI_Send(&nb_voisins_in[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
        MPI_Send(&nb_voisins_out[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
        MPI_Send(voisins_in[i], nb_voisins_in[i], MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
        MPI_Send(voisins_out[i], nb_voisins_out[i], MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
        MPI_Send(&min_local[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
    }
}

/******************************************************************************/

/**
 * @brief all_sup_to Dis si toutes les valeurs contenues dans test sont plus grandes que value ou non
 */
int all_sup_to(int* test, int size, int value)
{
    int i;
    for (i = 0; i < size; i++)
        if(test[i] < value)
            return 0;

    return 1;
}

void calcul_min(int rank)
{
    int nb_voisins_in, nb_voisins_out;
    int *voisins_in, *voisins_out, *recu;
    int min_local, tmp_recv;
    int i;
    char sent = 0;
    MPI_Status status;

    //Reception des données du simulateur et allocation memoire
    MPI_Recv(&nb_voisins_in, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(&nb_voisins_out, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    voisins_in = (int*)malloc(nb_voisins_in * sizeof(int));
    recu = (int*)calloc(nb_voisins_in, sizeof(int));
    voisins_out = (int*)malloc(nb_voisins_out * sizeof(int));
    MPI_Recv(voisins_in, nb_voisins_in, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(voisins_out, nb_voisins_out, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(&min_local, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);

    while (!all_sup_to(recu, nb_voisins_in, DIAMETRE))
    {
        //Test send
        if ( all_sup_to(recu, nb_voisins_in, sent) && sent < DIAMETRE) {
            //Send
            for (i = 0; i < nb_voisins_out; i++)
                MPI_Send (&min_local, 1, MPI_INT, voisins_out[i], 1, MPI_COMM_WORLD);

            sent++;
        }

        //Reception
        MPI_Recv(&tmp_recv, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);

        //On cherche l'indice du proc dont on viens de recevoir
        for(i = 0; i < nb_voisins_in; i++)
            if(voisins_in[i] == status.MPI_SOURCE)
                break;

        //Traitement reception
        recu[i]++;

        //Actualisation du minimum
        min_local = (min_local < tmp_recv) ? min_local : tmp_recv;
    }

    printf("[%d] :: Decision: %d\n", rank, min_local);
}

int main (int argc, char* argv[]) {
    int nb_proc,rang;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nb_proc);

    if (nb_proc != NB_SITE+1) {
        printf("Nombre de processus incorrect !\n");
        MPI_Finalize();
        exit(2);
    }

    MPI_Comm_rank(MPI_COMM_WORLD, &rang);

    if (rang == 0) {
        simulateur();
    } else {
        calcul_min(rang);
    }

    MPI_Finalize();
    return 0;
}
