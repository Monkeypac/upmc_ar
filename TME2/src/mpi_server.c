#include "mpi_server.h"

static server the_server;

//ICI CODE THREAD SERVER A COM
void thread_serveur()
{
    int flag = 0;
    MPI_Status status;

    while(1)
    {

        while(!flag){
            serv_lock();
            MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
            serv_unlock();
        }

        the_server.callback(status.MPI_TAG, status.MPI_SOURCE);
    }
}

void start_server(void (*callback)(int, int))
{
    the_server.callback = callback;
    pthread_mutex_init(&the_server.mutex, NULL);
    pthread_create(&(the_server.listener), NULL, thread_serveur, NULL);
}

void destroy_server()
{
    pthread_kill(the_server.listener, SIGKILL);
    pthread_mutex_destroy(&the_server.mutex);
}

pthread_mutex_t* getMutex()
{
    return &(the_server.mutex);
}

void serv_lock()
{
    pthread_mutex_lock(&the_server.mutex);
}

void serv_unlock()
{
    pthread_mutex_unlock(&the_server.mutex);
}
