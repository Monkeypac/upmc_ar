#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

#include "mpi_server.h"

#define TAKE 0
#define GIVE 1
#define END 2

#define LEFT 0
#define RIGHT 1

#define true 1 //True use means "I'm about to use it" or "I'm using it"
#define false 0 //False means "I don't care, take it if ya wan'"

#define THINK 0
#define HUNGRY 1
#define EAT 2
#define OVER 3

int rank, nproc;
int state;
int bag[2];
int neigh[2];

void ask(int who)
{
    int tmp;

    //Ask dir to give the bag
    serv_lock();
    MPI_Send(&tmp, 1, MPI_INT, who, TAKE, MPI_COMM_WORLD);
    serv_unlock();
}

void give(int who, int what)
{
    int tmp = what;
    //Give the bag to dir
    serv_lock();
    MPI_Send(&tmp, 1, MPI_INT, who, GIVE, MPI_COMM_WORLD);
    serv_unlock();
}

void handle_msg(int tag, int src)
{
    MPI_Status status;
    int tmp;

    switch(tag)
    {
    case TAKE:
        if(state == HUNGRY && rank > src) //Check priorities
        {
            give(src, false);
        }
        give(src, true);
        break;
    case GIVE:
        serv_lock();
        MPI_Recv(&tmp, 1, MPI_INT, src, tag, MPI_COMM_WORLD, &status);
        serv_unlock();
        if(tmp)
            bag[tmp] = true;
        else
            ask(src);
        break;
    case END:
        break;
    }
}

void showState(int _state)
{
    state = _state;
    switch(state)
    {
    case THINK:
        printf("[%d] I'm thinking\n", rank);
        break;
    case HUNGRY:
        printf("[%d] I'm hungry\n", rank);
        break;
    case EAT:
        printf("[%d] I'm eating\n", rank);
        break;
    case OVER:
        printf("[%d] I'm over\n", rank);
        break;
    }
}

/*
 * Just sleep some time
 */
void think()
{
    int sleep_time = rand() % 5;
    int i;

    showState(THINK);

    sleep(sleep_time);
}

/*
 * Well guys, I'M A FIR-- HUNGRY !
 */
void hungry()
{
    showState(HUNGRY);

    while (! (bag[LEFT] && bag[RIGHT]) ){
        //Ask for LEFT and RIGHT
        if (!bag[LEFT])
            ask(neigh[LEFT]);
        if (!bag[RIGHT])
            ask(neigh[RIGHT]);
    }
}

void eat()
{
    showState(EAT);
}

void over()
{
    showState(OVER);

    destroy_server();
}

/**
 * @brief run here everything takes place ~= main
 * @param nb_eats
 */
void myrun(int nb_eats)
{
    int i;

    //First start server in order to handle reception messages
    start_server(handle_msg);

    //While i didn't eat enough
    for(i = 0; i < nb_eats; i++)
    {
        think();
        hungry();
    }

    //Ok i'm done =3
    over();
}

int main(int argc, char **argv)
{
    int prov;
    if(argc < 2)
        exit(0);

    MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &prov);
    //MPI_Init(&argc, &argv);

    if(prov == MPI_THREAD_SERIALIZED)
        printf("I got the right one !\n");

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    srand(time(NULL) + rank);

    //Init
    bag[0] = 0;
    bag[1] = 0;
    neigh[0] = (rank + nproc - 1) % nproc;
    neigh[1] = (rank + 1) % nproc;

    printf("Let's run this!\n");
    myrun(atoi(argv[1]));

    MPI_Finalize();

    return 0;
}
