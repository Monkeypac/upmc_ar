#ifndef MPI_SERVER_H
#define MPI_SERVER_H

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <mpi.h>

typedef struct server{
    pthread_t listener;
    pthread_mutex_t mutex;
    void (*callback)(int tag, int source);
}server;

void start_server(void (*callback)(int, int));
void destroy_server();
pthread_mutex_t* getMutex();
void serv_lock();
void serv_unlock();

#endif
