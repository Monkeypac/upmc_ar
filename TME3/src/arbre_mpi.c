#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define TAGINIT    0
#define NB_SITE 6

void simulateur(void) {
   int i;

   /* nb_voisins[i] est le nombre de voisins du site i */
   int nb_voisins[NB_SITE+1] = {-1, 2, 3, 2, 1, 1, 1};
   int min_local[NB_SITE+1] = {-1, 3, 11, 8, 14, 5, 17};

   /* liste des voisins */
   int voisins[NB_SITE+1][3] = {{-1, -1, -1},
         {2, 3, -1}, {1, 4, 5}, 
         {1, 6, -1}, {2, -1, -1},
         {2, -1, -1}, {3, -1, -1}};
                               
   for(i=1; i<=NB_SITE; i++){
      MPI_Send(&nb_voisins[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);    
      MPI_Send(voisins[i],nb_voisins[i], MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
      MPI_Send(&min_local[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD); 
   }
}

int non_zero(int*voisins, char*test, int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
        if(!test[i]){
            test[i] = 1;
            return voisins[i];
        }
    }
    return -1;
}

void calcul_min(int rank)
{
    int nb_voisins;
    int *voisins;
    int min_local;
    int i, last = 0;
    char sent = 0, *recu, nb_recu = 0;
    MPI_Status status;

    MPI_Recv(&nb_voisins, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    voisins = (int*)malloc(nb_voisins * sizeof(int));
    recu = (char*)calloc(nb_voisins, sizeof(char));
    MPI_Recv(voisins, nb_voisins, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(&min_local, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);

    printf("[%d] :: %d voisins(%d", rank, nb_voisins, voisins[0]);
    for(i = 1; i < nb_voisins; i++)
        printf(", %d", *(voisins+i));
    printf(") min_local %d\n", min_local);

    while(nb_voisins != nb_recu)
    {
        //Send
        if (sent == 0 && nb_recu == (nb_voisins - 1))
        {
            MPI_Send(&min_local, 1, MPI_INT, non_zero(voisins, recu, nb_voisins), 1, MPI_COMM_WORLD);
            sent = 1;
        }
        //Recv && calc min
    int tmp;
    MPI_Recv(&tmp, 1, MPI_INT, MPI_ANY_TAG, 1, MPI_COMM_WORLD, &status);
    last = status.MPI_SOURCE;
    for(i = 0; i < nb_voisins; i++)
    	if(voisins[i] == status.MPI_SOURCE)
      	  break;
    recu[i] = 1;
    nb_recu++;
    min_local = (tmp < min_local) ? tmp : min_local;
    }
    

    //Decide
    for(i = 0; i < nb_voisins; i++)
    {
        if(voisins[i] != last)
            MPI_Send(&min_local, 1, MPI_INT, voisins[i], 1,MPI_COMM_WORLD);
    }
    printf("[%d] : Min %d\n", rank, min_local);
}

/******************************************************************************/

int main (int argc, char* argv[]) {
   int nb_proc,rang;
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &nb_proc);

   if (nb_proc != NB_SITE+1) {
      printf("Nombre de processus incorrect !\n");
      MPI_Finalize();
      exit(2);
   }
  
   MPI_Comm_rank(MPI_COMM_WORLD, &rang);
  
   if (rang == 0) {
      simulateur();
   } else {
      calcul_min(rang);
   }
  
   MPI_Finalize();
   return 0;
}
