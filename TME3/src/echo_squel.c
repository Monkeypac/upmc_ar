#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define TAGINIT 0
#define NB_SITE 6
#define INIT 1

void simulateur(void) {
   int i;

   /* nb_voisins[i] est le nombre de voisins du site i */
   int nb_voisins[NB_SITE+1] = {-1, 3, 3, 2, 3, 5, 2};
   int min_local[NB_SITE+1] = {-1, 12, 11, 8, 14, 5, 17};

   /* liste des voisins */
   int voisins[NB_SITE+1][5] = {{-1, -1, -1, -1, -1},
            {2, 5, 3, -1, -1}, {4, 1, 5, -1, -1}, 
            {1, 5, -1, -1, -1}, {6, 2, 5, -1, -1},
            {1, 2, 6, 4, 3}, {4, 5, -1, -1, -1}};
                               
   for(i=1; i<=NB_SITE; i++){
      MPI_Send(&nb_voisins[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);    
      MPI_Send(voisins[i], nb_voisins[i], MPI_INT, i, TAGINIT, MPI_COMM_WORLD);    
      MPI_Send(&min_local[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD); 
   }
}

int non_zero(int*voisins, char*test, int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
        if(!test[i]){
            test[i] = 1;
            return voisins[i];
        }
    }
    return -1;
}

void calcul_min(int rank){
    int nb_voisins;
    int * voisins;
    int min, tmp_min;
    int from =-1;
    int i;
    MPI_Status status;

    MPI_Recv(&nb_voisins, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    voisins=(int*)malloc(sizeof(int)*nb_voisins);
    MPI_Recv(voisins, nb_voisins, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(&min, 1, MPI_INT, 0, TAGINIT, MPI_COMM_WORLD, &status);

    printf("[%d] :: Start min %d\n", rank, min);

    //Start echo
    if (rank != INIT) {
        MPI_Recv(&tmp_min, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
        from = status.MPI_SOURCE;

        if (tmp_min < min)
            min = tmp_min;
    }

    for (i=0; i < nb_voisins; i++)
        if (voisins[i] != from)
            MPI_Send(&min, 1, MPI_INT, voisins[i], 1, MPI_COMM_WORLD);

    // Recoit les min locaux à chaque voisin et ajuste min local
    for (i = 0; i < (nb_voisins - ((rank == INIT) ? 0 : 1)); i++){
        MPI_Recv(&tmp_min, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);

        if (tmp_min < min)
            min = tmp_min;
    }

    //Decide
    if (rank != INIT) {
        MPI_Send(&min, 1, MPI_INT, from, 1, MPI_COMM_WORLD);
        MPI_Recv(&min, 1, MPI_INT, from, TAGINIT, MPI_COMM_WORLD, &status);
    }
    for (i=0; i < nb_voisins; i++)
        if (voisins[i] != from)
            MPI_Send(&min, 1, MPI_INT, voisins[i], TAGINIT, MPI_COMM_WORLD);

    printf("[%d] :: Final min %d\n", rank, min);

}

/******************************************************************************/

int main (int argc, char* argv[]) {
   int nb_proc,rang;
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &nb_proc);

   if (nb_proc != NB_SITE+1) {
      printf("Nombre de processus incorrect !\n");
      MPI_Finalize();
      exit(2);
   }
  
   MPI_Comm_rank(MPI_COMM_WORLD, &rang);
  
   if (rang == 0) {
      simulateur();
   } else {
      calcul_min(rang);
   }
  
   MPI_Finalize();
   return 0;
}
