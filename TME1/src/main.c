#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <pthread.h>
#include "mpi_server.h"

pthread_cond_t cond;

void recv(int tag, int src)
{
    char data[100];
    pthread_mutex_lock(getMutex());
    MPI_Recv(data, 100, MPI_CHAR, src, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    pthread_mutex_unlock(getMutex());

    printf("%s\n", data);
}

int main(int argc, char **argv)
{
    int rank, nproc;
    int provided;
    char data[100];

    printf("MPI_Init_thread\n");
    MPI_Init_thread(&argc, &argv, MPI_THREAD_SINGLE, &provided);
    printf("Init: %d\n", provided);

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    sprintf(data, "Message de %d à %d", rank, (rank+1)%nproc);

    printf("Start!\n"); fflush(stdout);
    start_server(recv);

    printf("Will lock send\n");
    pthread_mutex_lock(getMutex());
    printf("Will send\n");
    MPI_Send(data, strlen(data) + 1, MPI_CHAR, (rank+1)%nproc, 1, MPI_COMM_WORLD);
    pthread_mutex_unlock(getMutex());

    destroy_server();

    MPI_Finalize();

    return 0;
}
