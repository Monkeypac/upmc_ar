#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <unistd.h>

#define SIZE_H_N 50

int main(int argc, char**argv)
{
    int rank, nproc;
    int src, dest;
    int tag = 0;
    char message[100];
    MPI_Status status;
    char hostname[SIZE_H_N];

    gethostname(hostname, SIZE_H_N);

    /* Initialisation */
    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank)
    {
        sprintf(message, "Coucou du processus #%d depuis %s!", rank, hostname);
        dest = 0;
        MPI_Send(message, strlen(message) +1, MPI_CHAR, dest, tag, MPI_COMM_WORLD);
    }
    else
    {
        for(src = 1; src < nproc; src++)
        {
            //MPI_Recv(message, 100, MPI_CHAR, src, tag, MPI_COMM_WORLD, &status);
            MPI_Recv(message, 100, MPI_CHAR, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
            printf("Sur %s, le processus #%d a recu le message : %s\n", hostname, rank, message);
        }
    }

    MPI_Finalize();

    return 0;
}
