#include "mpi_server.h"

static server the_server;

//ICI CODE THREAD SERVER A COM
void thread_serveur()
{
    int flag = 0;
    MPI_Status status;

    while(!flag){
        printf("Will probe\n");
        pthread_mutex_lock(getMutex());
        MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
        pthread_mutex_unlock(getMutex());
    }

    the_server.callback(status.MPI_TAG, status.MPI_SOURCE);
}

void start_server(void (*callback)(int, int))
{
    printf("1\n");
    the_server.callback = callback;
    printf("2\n");
    pthread_mutex_init(&the_server.mutex, NULL);
    printf("3\n");
    pthread_create(&(the_server.listener), NULL, thread_serveur, NULL);
    printf("4\n");
}

void destroy_server()
{
    pthread_join(the_server.listener, NULL);
    pthread_mutex_destroy(&the_server.mutex);
}

pthread_mutex_t* getMutex()
{
    return &(the_server.mutex);
}
