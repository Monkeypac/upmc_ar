#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

#define REQ 0
#define ACK 1
#define END 2

#define REQUESTING 0
#define NOT_REQUESTING 1
#define CS 2

#define NB_CS 6

typedef struct fifo
{
    int v;
    struct fifo* next;
}fifo;

fifo* new_fifo(int v)
{
    fifo* f = (fifo*)malloc(sizeof(fifo));
    f->v = v;
    f->next = NULL;

    return f;
}

void add_fifo(int v, fifo** f)
{
    fifo* el = (fifo*)malloc(sizeof(fifo));
    el->v = v;
    el->next = *f;
    *f = el;
}

void bcast_fifo(int h, fifo** f)
{
    fifo* tmp = *f, *to_free;
    while (tmp != NULL) {
        if (tmp->v != -1) {
            MPI_Send(&h, 1, MPI_INT, tmp->v, ACK, MPI_COMM_WORLD);
        }
        else {
            break;
        }
        to_free = tmp;
        tmp = tmp->next;
        free(to_free);
    }

    *f = NULL;
}

int main(int argc, char **argv)
{
    int nproc, rank;
    int nb_end, nb_ack, h=0, date_req, etat, rec_h;
    int cs_count, i;
    fifo* f = new_fifo(-1);
    MPI_Status status;

    //sleep(5);
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    for (cs_count = 0; cs_count < NB_CS; cs_count++) {
        //Demander
        h++;
        date_req = h;
        etat = REQUESTING;
        nb_ack = 0;
        for(i = 0; i < nproc; i++)
            if(i != rank)
                MPI_Send(&h, 1, MPI_INT, i, REQ, MPI_COMM_WORLD);

        //Recevoir tant qu'on a pas acces à la CS
        while( (nb_ack < (nproc - 1) ) && (nb_end < (nproc - 1) ) ) {
            MPI_Recv(&rec_h, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            h = ((rec_h < h) ? h : rec_h) + 1;

            switch(status.MPI_TAG) {
            case REQ:
                if (etat == NOT_REQUESTING || (etat == REQUESTING && (rec_h < date_req || (date_req == rec_h &&  status.MPI_SOURCE < rank) ) ) )
                    MPI_Send(&h, 1, MPI_INT, status.MPI_SOURCE, ACK, MPI_COMM_WORLD);
                else
                    add_fifo(status.MPI_SOURCE, &f);
                break;
            case ACK:
                nb_ack++;
                break;
            case END:
                nb_end ++;
                break;
            }
        }

        etat = CS;
        //Arrivée en CS

        printf("[%d] CS.\n", rank);

        //Fin CS
        h++;
        bcast_fifo(h, &f);
        etat = NOT_REQUESTING;
        //f = NULL already done by bcast_fifo
    }

    printf("[%d] J'attend que les autres finissent.\n", rank);
    for(i = 0; i < nproc; i++)
        if(i != rank)
            MPI_Send(&h, 1, MPI_INT, i, END, MPI_COMM_WORLD);

    while (nb_end != nproc - 1)
    {
        MPI_Recv(&rec_h, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        h = (rec_h < h) ? h : rec_h;

        switch(status.MPI_TAG) {
        case REQ:
            MPI_Send(&h, 1, MPI_INT, status.MPI_SOURCE, ACK, MPI_COMM_WORLD);
            break;
        case END:
            nb_end ++;
            break;
        }
    }

    printf("[%d] Fin.\n", rank);

    MPI_Finalize();
    return 0;
}
